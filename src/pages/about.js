import React from "react"

import "typeface-roboto-condensed"

import Image from "../components/image"
import SEO from "../components/seo"
import Layout from "../components/layout"
import Header from "../components/header"

const AboutPage = () => (
  <div>
  <Header/>
    <div class="page-content">
      <SEO title="About"/>
      <h1 class="about-heading">Who?</h1>

      <p class="about-text">I am JP</p>

      <p class="about-text">Based in Limerick, Ireland</p>

    </div>
  </div>

 
)

export default AboutPage
