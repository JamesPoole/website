import React from "react"
import { Link } from "gatsby"

import SEO from "../components/seo"
import Header from "../components/header"

const ProjectsPage = () => (
  <div>
    <Header/>
    <div class="page-content">
      <SEO title="Projects" />
      <h1 class="about-heading">Projects</h1>
    </div>
  </div>
)

export default ProjectsPage
