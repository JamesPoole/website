import React from "react"
import { Link } from "gatsby"
import "typeface-roboto-condensed"
import "typeface-eb-garamond"
import "typeface-nunito"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <div class="home-content">
    <SEO title="Home"/>

    <h1 class="intro-title">James Poole</h1>
    <p class="intro-subtitle">This is my internet</p>
    <Image/>
    <div class="intro-sublinks">
      <h3><a class="intro-link" href="/about/">James who?</a></h3>
      <h3><a class="intro-link" href="/projects/">Projects</a></h3>
      <h3><a class="intro-link" href="/blog/">Blog</a></h3>
    </div>

  </div>
)

export default IndexPage
